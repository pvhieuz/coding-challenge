//
//  Moment+CoreDataClass.h
//  codingchallenge-ios-objc
//
//  Created by Hieu Pham on 5/8/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class User;

NS_ASSUME_NONNULL_BEGIN

@interface Moment : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Moment+CoreDataProperties.h"
