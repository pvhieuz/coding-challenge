//
//  User+CoreDataProperties.h
//  codingchallenge-ios-objc
//
//  Created by Hieu Pham on 5/8/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import "User+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface User (CoreDataProperties)

+ (NSFetchRequest<User *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *imageURL;
@property (nullable, nonatomic, copy) NSDate *dob;
@property (nullable, nonatomic, copy) NSString *fullName;
@property (nullable, nonatomic, copy) NSString *mobileNo;
@property (nullable, nonatomic, copy) NSString *userID;
@property (nullable, nonatomic, retain) NSSet<Moment *> *moments;

@end

@interface User (CoreDataGeneratedAccessors)

- (void)addMomentsObject:(Moment *)value;
- (void)removeMomentsObject:(Moment *)value;
- (void)addMoments:(NSSet<Moment *> *)values;
- (void)removeMoments:(NSSet<Moment *> *)values;

@end

NS_ASSUME_NONNULL_END
