//
//  Moment+CoreDataProperties.m
//  codingchallenge-ios-objc
//
//  Created by Hieu Pham on 5/8/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import "Moment+CoreDataProperties.h"

@implementation Moment (CoreDataProperties)

+ (NSFetchRequest<Moment *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Moment"];
}

@dynamic comment;
@dynamic imageURL;
@dynamic like;
@dynamic message;
@dynamic momentID;
@dynamic postedAt;
@dynamic lastUpdate;
@dynamic hasLiked;
@dynamic user;

@end
