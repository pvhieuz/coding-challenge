//
//  Moment+CoreDataProperties.h
//  codingchallenge-ios-objc
//
//  Created by Hieu Pham on 5/8/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import "Moment+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Moment (CoreDataProperties)

+ (NSFetchRequest<Moment *> *)fetchRequest;

@property (nonatomic) int32_t comment;
@property (nullable, nonatomic, copy) NSString *imageURL;
@property (nonatomic) int32_t like;
@property (nullable, nonatomic, copy) NSString *message;
@property (nullable, nonatomic, copy) NSString *momentID;
@property (nullable, nonatomic, copy) NSDate *postedAt;
@property (nullable, nonatomic, copy) NSDate *lastUpdate;
@property (nonatomic) BOOL hasLiked;
@property (nullable, nonatomic, retain) User *user;

@end

NS_ASSUME_NONNULL_END
