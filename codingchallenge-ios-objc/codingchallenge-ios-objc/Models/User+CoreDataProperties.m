//
//  User+CoreDataProperties.m
//  codingchallenge-ios-objc
//
//  Created by Hieu Pham on 5/8/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import "User+CoreDataProperties.h"

@implementation User (CoreDataProperties)

+ (NSFetchRequest<User *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"User"];
}

@dynamic imageURL;
@dynamic dob;
@dynamic fullName;
@dynamic mobileNo;
@dynamic userID;
@dynamic moments;

@end
