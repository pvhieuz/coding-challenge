//
//  ServerManager.m
//  codingchallenge-ios-objc
//
//  Created by Hieu Pham on 5/8/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import "ServerManager.h"
#import "InternetReachability.h"
#import "AppDelegate.h"


@implementation ServerManager

//Check network is available
+ (BOOL) connectionAvailable{
    InternetReachability *reachability = [InternetReachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

+ (BOOL) noConnection {
    
    if (![ServerManager connectionAvailable]) {
        return YES;
    }
    
    return NO;
}

//Template method
+(NSError*)generateErrorCode:(int)code description:(NSString*)description{
    return  [NSError errorWithDomain:@"some_domain"
                                       code:code
                                   userInfo:@{
                                              NSLocalizedDescriptionKey:description
                                              }];

}

//Use for get like or comment
+(void)makeGETrequestUserActivityWithBaseURL:(NSString *)baseURL
                        endpoint:(NSString *)endpoint
                      completion:(JSONcompletion)completion{
    [self makeGETrequestWithBaseURL:baseURL
                           endpoint:endpoint
                         completion:^(BOOL success, NSDictionary *dict, NSError *error) {
                             if(completion){
                                 if (success) {
                                     completion(success, dict, nil);
                                 }else{
                                     completion(NO, nil, error);
                                 }
                             }
                         }];
}

+ (void)makeGETrequestWithBaseURL: (NSString *)baseURL
                          endpoint: (NSString *)endpoint
                        completion: (JSONcompletion) completion{

    if ([self noConnection]){
        if(completion){
            NSString *errDescription = @"No internet connection.";
            NSError *err = [self generateErrorCode:100 description:errDescription];
            completion(NO,nil,err);

        }
        return;
    }
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",baseURL,endpoint];
    
    DLog(@"\nGET request made to URL: \n%@", urlString);
    
    // making a GET request to /init
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:
      ^(NSData * _Nullable data,
        NSURLResponse * _Nullable response,
        NSError * _Nullable error) {
          
          if(error || !data){
              if(completion){
                  NSString *errDescription = @"Something went wrong";
                  NSError *err = [self generateErrorCode:101 description:errDescription];
                  completion(NO,nil,err);
                  
              }
              
              return ;
          }
          
          NSError *_error = nil;
          id object = [NSJSONSerialization
                       JSONObjectWithData:data
                       options:0
                       error:&_error];
          
          //JSON was malformed, act appropriately here
          if(_error) {
              if (completion){
                  if(completion){
                      NSString *errDescription = @"Something went wrong";
                      NSError *err = [self generateErrorCode:102 description:errDescription];
                      completion(NO,nil,err);
                      
                  }

              }
                  
              
              return;
          }
          
          // the originating poster wants to deal with dictionaries;
          // assuming you do too then something like this is the first
          // validation step:
          if([object isKindOfClass:[NSDictionary class]])
          {
             
              //Makesure code == 200 and status == success
              NSInteger code = [[object valueForKey:@"code"]  integerValue];
              NSString *status = [object valueForKey:@"status"];
              if(code == 200 && [status isEqualToString:@"success"]){
                  if (completion)
                      completion(YES, object ,nil);
                  DLog(@"Data received: %@", object);
              }else{
                  if(completion){
                      NSString *errDescription = @"Something went wrong";
                      NSError *err = [self generateErrorCode:103 description:errDescription];
                      completion(NO,nil,err);
                      
                  }
              }
              
              
          }else{
              if(completion){
                  NSString *errDescription = @"Something went wrong";
                  NSError *err = [self generateErrorCode:101 description:errDescription];
                  completion(NO,nil,err);
                  
              }
          }
          
          
      }] resume];
    
    DLog(@"\nGET request Headers: %@", request.allHTTPHeaderFields);
    
}

+(void)saveMomentToLocal:(NSDictionary *)data{
    NSArray *posts = [data objectForKey:@"data"];
    DLog(@"All post received: %@", posts);
    
    //Keep all Moment model
    NSMutableArray *moments = [[NSMutableArray alloc] initWithCapacity:[posts count]];
    BOOL needUpdate = NO;
    NSManagedObjectContext *context = [Appdelegate managedObjectContext];
    for(NSDictionary *post in posts){
        DLog(@"Post received: %@", post);
        
        NSDictionary *user = [post valueForKey:@"user"];
        
        //Post info
        NSString *postID = [NSString stringWithFormat:@"%@",[post valueForKey:@"_id"]];
        NSString *postImageURL = [NSString validString:[post valueForKey:@"imageURL"] _default:@""];
        NSString *postMessage = [NSString validString:[post valueForKey:@"message"] _default:@""];
        NSString *postedAt = [NSString validString:[post valueForKey:@"postedAt"] _default:@"1970-02-17T00:00:08+00:00"];
        NSDate *postDate = [NSDate dateFromString:postedAt];
        
        //User info
        NSString *userID = [NSString stringWithFormat:@"%@",[user valueForKey:@"_id"]];
        NSString *_userDob = [NSString validString:[user valueForKey:@"dob"] _default:@"1970-02-17T00:00:08+00:00"];
        NSDate *userDob = [NSDate dateFromString:_userDob];
        NSString *userFullName = [NSString validString:[user valueForKey:@"fullName"] _default:@""];
        NSString *userImageURL = [NSString validString:[user valueForKey:@"imageURL"] _default:@""];
        NSString *userMobileNo = [NSString validString:[user valueForKey:@"mobileNo"] _default:@""];
        
        
        
        //FIND EXISTING OBJECT OR CREATE NEW OBJECT
        NSPredicate *predicate = [NSPredicate predicateWithFormat:
                                  @"momentID == %@", postID];
        Moment *momentObj = [Moment findFirstWithPredicate:predicate inContext:context];
        if(!momentObj){
            momentObj = [Moment createEntityInContext:context];
        }
        
        //DO NOT MODIFY OBJ IF ALL PROPERTIES HAVE NOT CHANGED --> Prevents flashing on server response when using core data table
        if(![postID isEqualToString:momentObj.momentID] ||
           ![postImageURL isEqualToString:momentObj.imageURL] ||
           ![postMessage isEqualToString:momentObj.message] ||
           ![postDate isEqual:momentObj.postedAt]){
            //update
            momentObj.momentID = postID;
            momentObj.imageURL = postImageURL;
            momentObj.message = postMessage;
            momentObj.postedAt = postDate;
            momentObj.hasLiked = NO;
            momentObj.like = momentObj.like > 0 ? momentObj.like : 0;
            momentObj.comment = momentObj.comment > 0 ? momentObj.comment : 0;
            
            predicate = [NSPredicate predicateWithFormat:
                                      @"userID == %@", userID];
            User *userObj = [User findFirstWithPredicate:predicate inContext:context];
            if(!userObj){
                userObj = [User createEntityInContext:context];
                userObj.userID = userID;
                userObj.mobileNo = userMobileNo;
                userObj.dob = userDob;
                userObj.imageURL = userImageURL;
                userObj.fullName = userFullName;
  
            }
            momentObj.user = userObj;
            needUpdate = YES;
        }
        [moments addObject:momentObj];
    }
    
    //Lock up old Moments and remove them.
    //FIND All Entities in local
    NSPredicate *predicate = [NSPredicate predicateWithValue:YES];
    NSMutableArray *oldMoments  = [NSMutableArray arrayWithArray:[Moment findAllWithPredicate:predicate inContext:[Appdelegate managedObjectContext]]];
    //The intersection of two collections is the entries that exist in both of them.
    NSMutableSet *oldMomentsNSSet = [NSMutableSet setWithArray: oldMoments];
    NSSet *newMomentsNSSet = [NSSet setWithArray: moments];
    [oldMomentsNSSet intersectSet: newMomentsNSSet];
    NSArray *intersecArray = [oldMomentsNSSet allObjects];
    
    //Relative complement
    //I remove the common entries from either array.
    [oldMoments removeObjectsInArray:intersecArray];
    
    if(oldMoments.count > 0){
        needUpdate = YES;
    }
    //DELETE THE old Moment
    for (Moment *deletedMoment in oldMoments){
        [deletedMoment deleteEntityInContext:[Appdelegate managedObjectContext]];
    }

    //I need lock up image data and delete them, but i dont do it
    
    
    if(needUpdate){
        //Save to local
        dispatch_async(dispatch_get_main_queue(), ^(void){
            //Run UI Updates
            [Appdelegate saveMasterContext];
        });
    
    }
    
}

@end
