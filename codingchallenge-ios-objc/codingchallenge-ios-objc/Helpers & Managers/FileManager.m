//
//  FileManager.m
//  codingchallenge-ios-objc
//
//  Created by Hieu Pham on 5/8/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import "FileManager.h"

@implementation FileManager

+ (NSURL *) pathURLForNewDirectory: (NSString*) directoryPath
                          fileName: (NSString*) fileName {
    if (!directoryPath || !fileName || fileName.length == 0) return nil; //ERROR PROTECTION
    
    if (directoryPath.length >= 1) {
        if ([[directoryPath substringFromIndex:MAX(directoryPath.length-1,0)] isEqualToString:@"/"]) {
            //REMOVE TRAILING slash (/) from prefix
            directoryPath = [directoryPath substringToIndex:(directoryPath.length-1)];
        }
    }
    
    [FileManager createDirectory:directoryPath];
    NSURL *pathURL = [FileManager pathURLForDirectory:directoryPath
                                             fileName:fileName];
    
    return pathURL;
}

+ (NSURL *) saveDataToDisk: (NSData *) data
                 directory: (NSString*) directoryPath
                  fileName: (NSString*) fileName {
    
    if (!data) return nil; //Error Protection
    
    NSURL *fileURL = [self pathURLForNewDirectory: directoryPath
                                         fileName: fileName];
    DLog(@"save data to path:%@",fileURL);
    [data writeToURL: fileURL
          atomically: YES];
    
    return fileURL;
}


+ (void) createDirectory: (NSString *) path {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    NSString *imagesPath = [documentsPath stringByAppendingPathComponent:path];
    if (![fileManager fileExistsAtPath:imagesPath]) {
        [fileManager createDirectoryAtPath:imagesPath
               withIntermediateDirectories:YES
                                attributes:nil
                                     error:nil];
    }
}

+ (NSURL *) pathURLForDirectory:(NSString *)path
                       fileName:(NSString *)fileName{
    if (!path || !fileName || fileName.length == 0) return nil; //ERROR PROTECTION
    
    if (path.length >= 1) {
        //TEMP: RECENTLY ADDED REMOVE BACKSLASH IF IT EXISTS
        if ([[path substringFromIndex:MAX(path.length-1,0)] isEqualToString:@"/"]) {
            //REMOVE TRAILING slash (/) from prefix
            path = [path substringToIndex:(path.length-1)];
        }
    }
    
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSURL *pathURL = [fileManager URLForDirectory:NSDocumentDirectory
                                         inDomain:NSUserDomainMask
                                appropriateForURL:nil
                                           create:YES
                                            error:nil];
    
    pathURL = [pathURL URLByAppendingPathComponent:path
                                       isDirectory:YES];
    pathURL = [pathURL URLByAppendingPathComponent:fileName];
    pathURL = [pathURL filePathURL];
    return pathURL;
}

+ (NSURL *) pathURLForBundle:(eBundle)bundleType
                    fileName:(NSString *)fileName
                    fileType:(NSString *)fileType {
    NSBundle *bundle = [NSBundle mainBundle];
    NSString *pathStr = [bundle pathForResource:fileName ofType:fileType];
    
    NSURL *pathURL = pathStr ? [NSURL fileURLWithPath:pathStr] : nil; //ERROR PROTECTION
    return pathURL;
    
}

+ (void) deleteFileAtURLPath:(NSURL *)pathURL {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error = nil;
    if ([fileManager fileExistsAtPath:[pathURL path]]) {
        
        if (![fileManager removeItemAtPath:[pathURL path]
                                     error:&error]) {
            NSLog(@"Error: %@", error);
        }
    }
}


+ (BOOL)isImageExsitWithName:(NSString *)name withPath:(NSString *)path{
    //Look for image locally
    NSString *fileName = [FileManager fileNameForImageName:name
                                            defaultExtension:@".jpg"];
    NSURL *imageURL = [FileManager pathURLForDirectory:path
                                              fileName:fileName];
    NSString *fileURLStr = imageURL.path;
    return [[NSFileManager defaultManager] fileExistsAtPath:fileURLStr];
}


+ (NSArray *) contentsOfDirectoryAtURL:(NSURL *)url {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *contents = [fileManager contentsOfDirectoryAtURL:url
                                   includingPropertiesForKeys:@[]
                                                      options:NSDirectoryEnumerationSkipsHiddenFiles
                                                        error:nil];
    
    return contents;
}


+ (void) deleteAllFilesInDirectory:(NSURL *)directoryURL {
    NSArray *fileURLs = [self contentsOfDirectoryAtURL:directoryURL];
    for (NSURL *url in fileURLs) {
        [self deleteFileAtURLPath:url];
    }
}

+ (void) deleteAllFilesInDocumentsDirectory {
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSURL *docURL = [fileManager URLForDirectory:NSDocumentDirectory
                                        inDomain:NSUserDomainMask
                               appropriateForURL:nil
                                          create:YES
                                           error:nil];
    [self deleteAllFilesInDirectory:docURL];
}

#pragma mark Cached Images
+ (void) saveImage: (UIImage *) image
              path: (NSString *) path
              name: (NSString *) name {
    
    //Store Image As PNG by default or JPG if extension matches
    NSData *imageData = nil;
    if (name.length > 4 &&
        [[name substringFromIndex:name.length-4] isEqualToString:@".jpg"])
        imageData = UIImageJPEGRepresentation(image, 0.8);
    else
        imageData = UIImagePNGRepresentation(image);
    

    
    NSString *fileName = [FileManager fileNameForImageName:name
                                          defaultExtension:@".jpg"];
    [FileManager saveDataToDisk:imageData
                      directory:path
                       fileName:fileName];
    
}

+ (UIImage *) imageAtPath: (NSString *) path
                     name: (NSString *) name {
    
    
    //Look for image locally
    NSString *fileName = [FileManager fileNameForImageName:name
                                          defaultExtension:@".jpg"];
    NSURL *imageURL = [FileManager pathURLForDirectory: path
                                              fileName: fileName];
    
    
    NSString *fileURLStr = imageURL.path;
    UIImage *image = [UIImage imageWithContentsOfFile:fileURLStr];
    return image;
}



+ (NSString *) fileNameForImageName: (NSString *) name
                   defaultExtension: (NSString *) extension {
    
    if (!name || name.length == 0)
        return nil;
    
    //GENERATE FILE NAME
    NSString *fileName = name;
    NSString *lastFour = @"";
    if (fileName.length >= 4)
        lastFour = [fileName substringFromIndex:(fileName.length-4)];
    
    lastFour = [lastFour lowercaseString];
    
    if (!([lastFour isEqualToString:@".png"] ||
          [lastFour isEqualToString:@".jpg"] ||
          [lastFour isEqualToString:@"jpeg"] ||
          [lastFour isEqualToString:@".tif"] ||
          [lastFour isEqualToString:@"tiff"])) {
        
        if (extension && extension.length > 0)
            fileName = [NSString stringWithFormat:@"%@%@",name,extension];
    }
    DLog(@"GENERATE FILE NAME: %@", fileName);
    return fileName;
}


@end
