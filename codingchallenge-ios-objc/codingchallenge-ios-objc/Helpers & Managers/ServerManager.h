//
//  ServerManager.h
//  codingchallenge-ios-objc
//
//  Created by Hieu Pham on 5/8/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Moment+CoreDataClass.h"
#import "User+CoreDataClass.h"

//Base URL
#define kBaseURL @"http://thedemoapp.herokuapp.com"

//Get all post
#define kAllPost @"/post"

//Get like count with given post id:
//Get /post/:post_id/likeCount
#define kLikeCount @"/post/%@/likeCount"

//Get comment count with given post id:
//Get /post/:post_id/commentCount
#define kCommentCount @"/post/%@/commentCount"

// in case success = NO, str is statusCode string
typedef void(^JSONcompletion)(BOOL success, NSDictionary *dict, NSError *error);

@interface ServerManager : NSObject


+ (void) makeGETrequestWithBaseURL: (NSString *)baseURL
                          endpoint: (NSString *)endpoint
                        completion: (JSONcompletion) completion;

+(void)makeGETrequestUserActivityWithBaseURL:(NSString *)baseURL
                        endpoint:(NSString *)endpoint
                      completion:(JSONcompletion)completion;
+(void)saveMomentToLocal:(NSDictionary*)data;

@end
