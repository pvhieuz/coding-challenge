//
//  FileManager.h
//  codingchallenge-ios-objc
//
//  Created by Hieu Pham on 5/8/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FileManager : NSObject
+ (NSURL *) pathURLForNewDirectory: (NSString*) directoryPath
                          fileName: (NSString*) fileName;

+ (NSURL *) saveDataToDisk: (NSData *) data
                 directory: (NSString*) directoryPath
                  fileName: (NSString*) fileName;

+ (void) createDirectory: (NSString *) path;


/**
 *  Retrieve an NSURL for a file and path in the NSDocumentDirectory
 *
 *  @param path     Local file path
 *  @param fileName Local file name
 *
 *  @return NSURL
 */
+ (NSURL *) pathURLForDirectory: (NSString *) path
                       fileName: (NSString*) fileName;

typedef enum {
    eBundleMain
} eBundle;
+ (NSURL *) pathURLForBundle: (eBundle) bundle
                    fileName: (NSString *) fileName
                    fileType: (NSString *) fileType;

+ (void) deleteFileAtURLPath: (NSURL *) pathURL;

+ (BOOL)isImageExsitWithName:(NSString *)name withPath:(NSString *)path;

+ (NSArray *) contentsOfDirectoryAtURL: (NSURL *) url; //Returns Array of NSURL Objects
+ (void) deleteAllFilesInDirectory: (NSURL *) directoryURL;
+ (void) deleteAllFilesInDocumentsDirectory;
//Cache Image
+ (void) saveImage: (UIImage *) image
              path: (NSString *) path
              name: (NSString *) name;
+ (UIImage *) imageAtPath: (NSString *) path
                     name: (NSString *) name;

@end
