//
//  ViewController.h
//  codingchallenge-ios-objc
//
//  Created by Tang Han on 19/2/16.
//  Copyright © 2016 User Experience Research. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
@interface ViewController : BaseViewController<UITableViewDelegate, UITableViewDataSource>


@end

