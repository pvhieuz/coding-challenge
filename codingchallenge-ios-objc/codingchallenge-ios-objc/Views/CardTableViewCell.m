//
//  CardTableViewCell.m
//  codingchallenge-ios-objc
//
//  Created by Hieu Pham on 5/8/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import "CardTableViewCell.h"
#import "AppDelegate.h"
@implementation CardTableViewCell

//Like action
- (IBAction)executeLike:(id)sender {
    if(self.momentObj.hasLiked){
        self.momentObj.like  = self.momentObj.like -1;
        self.likeIcon.image = [UIImage imageNamed:@"icon_moments_like_black"];
    }else{
        self.momentObj.like  = self.momentObj.like +1;
        self.likeIcon.image = [UIImage imageNamed:@"icon_moments_like_red"];
    }
    self.momentObj.like =  self.momentObj.like < 0 ? 0 : self.momentObj.like;
    self.momentObj.hasLiked = !self.momentObj.hasLiked;
    [self heartBeatAnimationOnView:self.likeIcon];
    [Appdelegate saveMasterContext];
}

//Comment action
- (IBAction)executeComment:(id)sender {

}

-(void)heartBeatAnimationOnView:(UIView*)view{
    CABasicAnimation *fade = [CABasicAnimation animationWithKeyPath:@"opacity"];
    fade.toValue = @0.5;
    CABasicAnimation *pulse = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    pulse.toValue = @2;
    CAAnimationGroup *group = [CAAnimationGroup animation];
    group.animations = @[fade,pulse];
    group.duration = 1.0;
    group.repeatCount = 0.5;
    [view.layer addAnimation:group forKey:@"g"];
}

@end
