//
//  CardTableViewCell.h
//  codingchallenge-ios-objc
//
//  Created by Hieu Pham on 5/8/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "Moment+CoreDataClass.h"
@interface CardTableViewCell : BaseTableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *userAvatar;

@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UILabel *postedAt;

@property (weak, nonatomic) IBOutlet UIImageView *mainImageView;

@property (weak, nonatomic) IBOutlet UILabel *message;

@property (weak, nonatomic) IBOutlet UIImageView *likeIcon;

@property (weak, nonatomic) IBOutlet UILabel *likeCount;

@property (weak, nonatomic) IBOutlet UIImageView *commentIcon;

@property (weak, nonatomic) IBOutlet UILabel *commentCount;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *userAvatarHeightLayoutConstraint;

@property(weak,nonatomic) Moment *momentObj;

@end
