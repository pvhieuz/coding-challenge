//
//  UIImageView+Utils.m
//  codingchallenge-ios-objc
//
//  Created by Hieu Pham on 5/8/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import "UIImageView+Utils.h"
#import "FileManager.h"
@implementation UIImageView (Utils)

- (NSBlockOperation*)operationImageWithURL: (NSString *) urlStr
                                      path: (NSString *) path
                                      name: (NSString *) name
                                completion:(OperationCompletion)operationCompletion{
    //First check for locally cached image
//    if ( path && name && name.length > 0){
//        BOOL isFileExsit = [FileManager isImageExsitWithName:name withPath:path];
//        if (isFileExsit){
//            UIImage *image = [FileManager imageAtPath:path name:name];
//            self.image = image;
//            return nil;
//        }
//    }
    
    //Create a block operation for loading the image into the  image view
    NSBlockOperation *loadImageIntoCellOp = [[NSBlockOperation alloc] init];
    //Define weak operation so that operation can be referenced from within the block without creating a retain cycle
    __weak NSBlockOperation *weakOp = loadImageIntoCellOp;
    [loadImageIntoCellOp addExecutionBlock:^(void){
        //Some asynchronous work. Once the image is ready, it will load into view on the main queue
        UIImage *loadImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:urlStr]]];
        //Catche image
        [FileManager saveImage:loadImage path:path name:name];
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^(void) {
            //Check for cancelation before proceeding. We use cellForRowAtIndexPath to make sure we get nil for a non-visible cell
            if (!weakOp.isCancelled) {
                [self setImage:loadImage];
                if(operationCompletion){
                    operationCompletion(loadImage);
                }
            }
        }];
    }];
    
    //Save a reference to the operation in an NSMutableDictionary so that it can be cancelled later on
    return loadImageIntoCellOp;
    
}
@end
