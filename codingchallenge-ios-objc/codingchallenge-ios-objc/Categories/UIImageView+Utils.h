//
//  UIImageView+Utils.h
//  codingchallenge-ios-objc
//
//  Created by Hieu Pham on 5/8/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^OperationCompletion)(UIImage *image);
@interface UIImageView (Utils)

/**
 *  Convenience method for setting an image using a url and optional path/name.
 *  This makes use of async to do the download.
 *  Images can be stored in a local cache.
 *
 *  @param urlStr      <#urlStr description#>
 *  @param path        <#path description#>
 *  @param name        <#name description#>
 */
- (NSBlockOperation*)operationImageWithURL: (NSString *) urlStr
                                      path: (NSString *) path
                                      name: (NSString *) name
                                completion:(OperationCompletion)operationCompletion;

@end
