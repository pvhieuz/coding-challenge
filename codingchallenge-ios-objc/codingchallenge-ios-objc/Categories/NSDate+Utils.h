//
//  NSDate+Utils.h
//  codingchallenge-ios-objc
//
//  Created by Hieu Pham on 5/8/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Utils)

//Convert String to NSDate
+ (NSDate *)dateFromString: (NSString *) string;

- (NSString *)formattedAsTimeAgo;
@end
