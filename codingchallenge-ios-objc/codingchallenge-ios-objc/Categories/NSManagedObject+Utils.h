//
//  NSManagedObject+Utils.h
//  codingchallenge-ios-objc
//
//  Created by Hieu Pham on 5/8/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObject (Utils)
+(instancetype)findFirstWithPredicate:(NSPredicate *)searchTerm inContext:(NSManagedObjectContext *)context;
+ (NSArray *) findAllWithPredicate:(NSPredicate *)searchTerm inContext:(NSManagedObjectContext *)context;
+ (id)createEntityInContext:(NSManagedObjectContext *)context;
- (BOOL)deleteEntityInContext:(NSManagedObjectContext *)context;
@end
