//
//  NSManagedObject+Utils.m
//  codingchallenge-ios-objc
//
//  Created by Hieu Pham on 5/8/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import "NSManagedObject+Utils.h"

@implementation NSManagedObject (Utils)

+ (id)executeFetchRequestAndReturnFirstObject:(NSFetchRequest *)request inContext:(NSManagedObjectContext *)context{
    [request setFetchLimit:1];
    
    NSArray *results = [self executeFetchRequest:request inContext:context];
    if ([results count] == 0)
    {
        return nil;
    }
    return [results firstObject];
}

+ (NSArray *)executeFetchRequest:(NSFetchRequest *)request inContext:(NSManagedObjectContext *)context{
    __block NSArray *results = nil;
    [context performBlockAndWait:^{
        
        NSError *error = nil;
        
        results = [context executeFetchRequest:request error:&error];
        
        if (results == nil)
        {
            DLog(@"Error executeFetchRequest");
            abort();
        }
        
    }];
    return results;
}

+ (NSFetchRequest *) requestFirstWithPredicate:(NSPredicate *)searchTerm inContext:(NSManagedObjectContext *)context{
    NSFetchRequest *request = [self createFetchRequestInContext:context];
    [request setPredicate:searchTerm];
    [request setFetchLimit:1];
    
    return request;
}

+ (NSEntityDescription *) entityDescriptionInContext:(NSManagedObjectContext *)context{
    NSString *entityName = NSStringFromClass(self);
    return [NSEntityDescription entityForName:entityName inManagedObjectContext:context];
}


+ (NSFetchRequest *)createFetchRequestInContext:(NSManagedObjectContext *)context{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[self entityDescriptionInContext:context]];
    
    return request;
}


+(instancetype)findFirstWithPredicate:(NSPredicate *)searchTerm inContext:(NSManagedObjectContext *)context{
    NSFetchRequest *request = [self requestFirstWithPredicate:searchTerm inContext:context];
    
    return [self executeFetchRequestAndReturnFirstObject:request inContext:context];
}

+ (NSArray *)findAllWithPredicate:(NSPredicate *)searchTerm inContext:(NSManagedObjectContext *)context{
    NSFetchRequest *request = [self createFetchRequestInContext:context];
    [request setPredicate:searchTerm];
    
    return [self executeFetchRequest:request
                              inContext:context];
}

+ (id)createEntityInContext:(NSManagedObjectContext *)context{
    
    return [NSEntityDescription
            insertNewObjectForEntityForName:NSStringFromClass(self)
            inManagedObjectContext:context];

}

- (BOOL)deleteEntityInContext:(NSManagedObjectContext *)context
{
    NSError *error = nil;
    NSManagedObject *inContext = [context existingObjectWithID:[self objectID] error:&error];
    if(error){
        DLog(@"Can't deleted objective %@", self);
        return NO;
    }
    
    [context deleteObject:inContext];
    
    return YES;
}

@end
