//
//  NSString+Utils.h
//  codingchallenge-ios-objc
//
//  Created by Hieu Pham on 5/8/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Utils)

//VALIDATOR //#TODO Add nilBehavior enum param to specify whether to convert nils to @"", the other way around, or to leave as is
+ (NSString *) validString: (id) str
                  _default: (NSString *) _default;
@end
