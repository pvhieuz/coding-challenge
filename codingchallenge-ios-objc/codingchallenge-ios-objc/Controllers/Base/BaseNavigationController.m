//
//  BaseNavigationController.m
//  codingchallenge-ios-objc
//
//  Created by Hieu Pham on 5/7/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import "BaseNavigationController.h"

@interface BaseNavigationController ()

@end

@implementation BaseNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //set default bar color
    float bValue = 217.0/255.0;
    UIColor *bColor = [UIColor colorWithRed:bValue green:bValue blue:bValue alpha:1.0];
    [self.navigationBar setBarTintColor:bColor];
    
    //set default status bar background color
    float sValue = 161.0/255.0;
    UIColor *sColor = [UIColor colorWithRed:sValue green:sValue blue:sValue alpha:1.0];
    [self setStatusBarBackgroundColor: sColor];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - status bar bg color
- (void)setStatusBarBackgroundColor:(UIColor *)color {
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = color;
        self.navigationBar.barStyle = UIBarStyleBlack;
    }
}


@end
