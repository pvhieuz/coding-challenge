//
//  BaseViewController.m
//  codingchallenge-ios-objc
//
//  Created by Hieu Pham on 5/7/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import "BaseViewController.h"



@interface BaseViewController ()
@property (strong, nonatomic) ClickOnButtonAction rightBarClickAction;
@property (strong, nonatomic) ClickOnButtonAction leftBarClickAction;
@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - config bar right button
-(void)configureRightBarItemWithImage:(UIImage*)image
                                action:(ClickOnButtonAction)action{
    if(!image){
        return;
    }
    //Right bar button
    UIButton* customRightButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [customRightButton setTintColor:[UIColor whiteColor]];
    [customRightButton setImage:image forState:UIControlStateNormal];
    if ([self respondsToSelector:@selector(rightBarItemAction:)]) {
        [customRightButton addTarget: self action:@selector(rightBarItemAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    [customRightButton sizeToFit];
    UIBarButtonItem* customRightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:customRightButton];
 
    self.navigationItem.rightBarButtonItem = customRightBarButtonItem;
    self.rightBarClickAction = action;
}
-(void)rightBarItemAction:(UIButton*)sender{
    
    if(self.rightBarClickAction){
        self.rightBarClickAction();
        DLog(@"Right bar button clicked");
    }

}

#pragma mark - config bar left button
-(void)configureLeftBarItemWithTitle:(NSString*)titleName
                                        action:(ClickOnButtonAction)action{
    
    //Left bar button
    UIButton* customLeftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [customLeftButton setFrame:CGRectMake(0, 0, 50, 40)];
    [customLeftButton setTitle:titleName forState:UIControlStateNormal];
    [customLeftButton setTintColor:[UIColor whiteColor]];
    customLeftButton.titleLabel.font = [UIFont systemFontOfSize:16];
 
    if ([self respondsToSelector:@selector(leftBarItemAction:)]) {
        [customLeftButton addTarget: self action:@selector(leftBarItemAction:) forControlEvents:UIControlEventTouchUpInside];
    }

    UIBarButtonItem* customLeftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:customLeftButton];
    self.navigationItem.leftBarButtonItem = customLeftBarButtonItem;
    self.leftBarClickAction = action;

}

-(void)leftBarItemAction:(UIButton*)sender{
    
    if(self.leftBarClickAction){
        self.leftBarClickAction();
        DLog(@"Left bar button clicked");
    }
    
}

@end
