//
//  BaseViewController.h
//  codingchallenge-ios-objc
//
//  Created by Hieu Pham on 5/7/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^ClickOnButtonAction)();

@interface BaseViewController : UIViewController

#pragma mark - config bar right button
-(void)configureRightBarItemWithImage:(UIImage*)image
                                action:(ClickOnButtonAction)action;

#pragma mark - config bar left button
-(void)configureLeftBarItemWithTitle:(NSString*)titleName
                                action:(ClickOnButtonAction)action;
@end
