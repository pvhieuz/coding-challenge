//
//  BaseNavigationController.h
//  codingchallenge-ios-objc
//
//  Created by Hieu Pham on 5/7/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseNavigationController : UINavigationController

@end
