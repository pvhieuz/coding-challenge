//
//  ViewController.m
//  codingchallenge-ios-objc
//
//  Created by Tang Han on 19/2/16.
//  Copyright © 2016 User Experience Research. All rights reserved.
//

#import "ViewController.h"
#import "CardTableViewCell.h"
#import "User+CoreDataClass.h"
#import "Moment+CoreDataClass.h"
#import "AppDelegate.h"
#import <QuartzCore/CALayer.h>
#import "FileManager.h"
#define kMomentPath @"assets/Moment/"


@interface ViewController ()<NSFetchedResultsControllerDelegate>

//Listening data update, delete or add
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

//Define an NSOperationQueue for running userprofile background operations
@property(nonatomic, strong) NSOperationQueue *userProfileImageLoadingOperationQueue;

//For storing references to specific operations dowload profile image
@property(nonatomic, strong)NSMutableDictionary *userProfileImageDownloadOperations;

//For storing references to specific operations dowload main image
@property(nonatomic, strong)NSMutableDictionary *mainImageDownloadOperations;

//Define an NSOperationQueue for running main_image background operations
@property(nonatomic, strong) NSOperationQueue *mainImageLoadingOperationQueue;



@property (weak, nonatomic) IBOutlet UITableView *tableView;

//For notification
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *notificationTopConstraintLayout;

@property (weak, nonatomic) IBOutlet UILabel *lblNotification;

@property(nonatomic,strong)UIRefreshControl *refreshController;
@property(nonatomic,assign)BOOL refreshing;

@end

@implementation ViewController

#warning I hardcode insert 190 rows to test, you can pull down to refresh to delete them.
-(void)insertMyself{
    //FIND EXISTING OBJECT OR CREATE NEW OBJECT
    NSMutableArray *a = [[NSMutableArray alloc] init];
    for(int i = 10; i<200; i ++){
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"momentID == %d", i];
    Moment *momentObj = [Moment findFirstWithPredicate:predicate inContext:[Appdelegate managedObjectContext]];
    if(!momentObj){
        momentObj = [Moment createEntityInContext:[Appdelegate managedObjectContext]];
        momentObj.momentID = [NSString stringWithFormat:@"%d",i];
        momentObj.imageURL = @"https://scontent.fsgn5-1.fna.fbcdn.net/v/t31.0-8/11174376_625410494227330_3539887743859965754_o.jpg?oh=7abb00c116d03630ba0a8968afeeb8db&oe=597FEBF8";
        momentObj.message = [NSString stringWithFormat:@"Row index %d ",i];
        momentObj.hasLiked = NO;
        momentObj.like = i + 3;
        momentObj.comment = i + 100;
        
        predicate = [NSPredicate predicateWithFormat:
                     @"userID == %d", i];
        User *userObj = [User findFirstWithPredicate:predicate inContext:[Appdelegate managedObjectContext]];
        if(!userObj){
            userObj = [User createEntityInContext:[Appdelegate managedObjectContext]];
            userObj.userID = [NSString stringWithFormat:@"%d",i];
            userObj.mobileNo = @"0965 085 003";
            userObj.dob = [NSDate date];
            userObj.imageURL = @"http://res.cloudinary.com/tom1199/image/upload/v1455813222/images_zthkr0.png";
            userObj.fullName = @"Pham Van Hieu";
            
        }
        momentObj.user = userObj;
        momentObj.postedAt = [NSDate date];
        [a addObject:momentObj];
    }
    }
    [Appdelegate saveMasterContext];

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    //show back and camera button on bar
    [self configurationItemsBar];
    
    [self configureUI];
    
    //fetch
    [self confugureFetchedResult];
    
    //init for user stories
    _userProfileImageDownloadOperations = [NSMutableDictionary new];
    _userProfileImageLoadingOperationQueue = [[NSOperationQueue alloc] init];
    
    //init for main image stories
    _mainImageDownloadOperations = [NSMutableDictionary new];
    _mainImageLoadingOperationQueue = [[NSOperationQueue alloc] init];
    
    //Finally, you can improve more performance by setting setMaxConcurrentOperationCount.
    [_mainImageLoadingOperationQueue setMaxConcurrentOperationCount:3];
    [_userProfileImageLoadingOperationQueue setMaxConcurrentOperationCount:3];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    //Download and update when screen start
    [self getAllPost];
}

- (void)viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    
    // Cancel all
    [self.userProfileImageLoadingOperationQueue cancelAllOperations];
    [self.mainImageLoadingOperationQueue cancelAllOperations];
}

- (void)viewDidUnload{
    [super viewDidUnload];
    self.fetchedResultsController = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - configuration items bar
-(void)configurationItemsBar{
    
    //Right bar item button
    UIImage *cameraIcon = [UIImage imageNamed:@"icon_moments_camera"];
    [self configureRightBarItemWithImage:cameraIcon action:^{
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Pham Van Hieu" message:@"I will insert 190 rows on table :)" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            [self insertMyself];
        }];
        
        [alertController addAction:okAction];
        [self presentViewController:alertController animated: YES completion: nil];
    }];
    
    //Left bar item button
    
    [self configureLeftBarItemWithTitle:@"Back" action:^{
        [self showAlertViewWithMessage:@"I hope see you soon."];
        
    }];
}


#pragma mark - configureUI
-(void)configureUI{
    //set title
    self.title = @"Moments";
    
    self.tableView.allowsSelection = NO;
    self.tableView.contentInset = UIEdgeInsetsMake(-45, 0, 0, 0);
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 410;

    //Config pulldowntorefresh
    _refreshController = [[UIRefreshControl alloc] init];
    [_refreshController addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
    [_tableView addSubview:_refreshController];
}

#pragma mark - Handle Refresh Method
-(void)handleRefresh : (id)sender{
    NSLog (@"Pull To Refresh Method Called");
    [self getAllPost];
}

#pragma configure fetch
-(void)confugureFetchedResult{
    NSError *error;
    if (![[self fetchedResultsController] performFetch:&error]) {
        // Update to handle the error appropriately.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        exit(-1);  // Fail
    }
    
    _refreshing = NO;
}


#pragma mark - Get all post
-(void)getAllPost{
    if(_refreshing) return;
    
    _refreshing = YES;
    [ServerManager makeGETrequestWithBaseURL:kBaseURL
                                    endpoint:kAllPost
                                  completion:^(BOOL success, NSDictionary *dict, NSError *error) {
                                      dispatch_async(dispatch_get_main_queue(), ^(void){
                                          //Run UI Updates
                                          if ([self.refreshController isRefreshing]){
                                              [self.refreshController endRefreshing];
                                          }
                                      });
                                      
                                      _refreshing = NO;
                                        if(success){
                                            [ServerManager saveMomentToLocal:dict];
                                        }else{
                                            dispatch_async(dispatch_get_main_queue(), ^(void){
                                                //Run UI Updates
                                                if(_notificationTopConstraintLayout.constant == -40){
                                                    NSString *sms = [error.userInfo valueForKey:NSLocalizedDescriptionKey];
                                                    [self showNotificationViewWith:sms];
                                                }
                                            });
                                        }
                                    }];
}

-(void)showNotificationViewWith:(NSString*)message{
    _lblNotification.text = message;
    _notificationTopConstraintLayout.constant = 0;
    [UIView animateWithDuration:0.5 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        _notificationTopConstraintLayout.constant = -35;
        [UIView animateWithDuration:0.5 delay:3.0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             [self.view layoutIfNeeded];
                         } completion:^(BOOL finished) {
                             _notificationTopConstraintLayout.constant = -40;
                         }];
    }];
}

#pragma mark - alertview
-(void)showAlertViewWithMessage:(NSString*)message{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Pham Van Hieu" message:message preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        NSLog(@"OK");
    }];
    
    [alertController addAction:okAction];
    [self presentViewController:alertController animated: YES completion: nil];
}

#pragma mark - Table view data source

- (void)configureCardCell:(CardTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    // Circle user avatar
    CALayer *userImageLayer = cell.userAvatar.layer;
    [userImageLayer setMasksToBounds:YES];
    userImageLayer.opaque = NO;
    [userImageLayer setCornerRadius:cell.userAvatar.frame.size.width/2];
    
    Moment *moment = [_fetchedResultsController objectAtIndexPath:indexPath];
    cell.momentObj = moment;
    cell.userName.text = moment.user.fullName;
    cell.message.text = moment.message;
    cell.postedAt.text = [moment.postedAt formattedAsTimeAgo];
    cell.likeIcon.image = moment.hasLiked ? [UIImage imageNamed:@"icon_moments_like_red"] : [UIImage imageNamed:@"icon_moments_like_black"];
    cell.likeCount.text = moment.like > 1 ? [NSString stringWithFormat:@"%d Likes", moment.like] : [NSString stringWithFormat:@"%d Like",moment.like];
    
    cell.commentCount.text = moment.comment > 1 ? [NSString stringWithFormat:@"%d Comments", moment.comment] : [NSString stringWithFormat:@"%d Comment",moment.comment];
    
    NSString *avatarName = [moment.user.imageURL stringByReplacingOccurrencesOfString:@"/" withString:@""];
    //cell.userAvatar.image = nil;
    //First check for locally cached image
    BOOL isFileExsit = [FileManager isImageExsitWithName:avatarName withPath:kMomentPath];
    if (isFileExsit){
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            //Background Thread
            UIImage *image = [FileManager imageAtPath:kMomentPath name:avatarName];
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                //Run UI Updates
                cell.userAvatar.image = image;
            });
        });
    
    }else{
        //==========================================================
        //Execute download user profile from url
        //Keep a block operation for loading the image into the profile image view
        __weak NSBlockOperation *pfOperation;

        pfOperation = [cell.userAvatar operationImageWithURL:moment.user.imageURL
                                                        path:kMomentPath
                                                        name:avatarName
                                                  completion:^(UIImage *image) {
                                                      //Check for cancelation before proceeding. We use cellForRowAtIndexPath to make sure we get nil for a non-visible cell
                                                      if (!pfOperation.isCancelled) {
                                                          [_userProfileImageDownloadOperations removeObjectForKey:moment.user.userID];
                                                      }
                                                      
                                                  }];
        
        // if NSBlockOperation return nil then already image on local
        if(pfOperation){
            //Save a reference to the operation in an NSMutableDictionary so that it can be cancelled later on
            if (moment.user.userID) {
                [_userProfileImageDownloadOperations setObject:pfOperation forKey:moment.user.userID];
            }
            
            //Add the operation to the designated background queue
            [_userProfileImageLoadingOperationQueue addOperation:pfOperation];
        }


    }
    
    
    //cell.mainImageView.image = nil;

    NSString *mainImageName = [moment.imageURL stringByReplacingOccurrencesOfString:@"/" withString:@""];
    //First check for locally cached image
    isFileExsit = [FileManager isImageExsitWithName:mainImageName withPath:kMomentPath];
    if (isFileExsit){
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            //Background Thread
            UIImage *image = [FileManager imageAtPath:kMomentPath name:mainImageName];
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                //Run UI Updates
                cell.mainImageView.image = image;
            });
        });
        
    }else{
        //=========================================================
        //Execute download main image from url
        //Keep a block operation for loading the image into the profile image view
        __weak NSBlockOperation *mainImageOperation;

        mainImageOperation = [cell.mainImageView operationImageWithURL:moment.imageURL
                                                                  path:kMomentPath
                                                                  name:mainImageName
                                                            completion:^(UIImage *image) {
                                                                //Check for cancelation before proceeding. We use cellForRowAtIndexPath to make sure we get nil for a non-visible cell
                                                                if (!mainImageOperation.isCancelled) {
                                                                    [_mainImageDownloadOperations removeObjectForKey:moment.momentID];
                                                                }
                                                                
                                                            }];
        // if NSBlockOperation return nil then already image on local
        if(mainImageOperation){
            //Save a reference to the operation in an NSMutableDictionary so that it can be cancelled later on
            if (moment.momentID) {
                [_mainImageDownloadOperations setObject:mainImageOperation forKey:moment.momentID];
            }
            
            //Add the operation to the designated background queue
            [_mainImageLoadingOperationQueue addOperation:mainImageOperation];
        }
    
    }
    
    
   

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
  
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
 
    // Return the number of sections.
    NSInteger number = [[_fetchedResultsController fetchedObjects] count];
    return number;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CardCell";
    CardTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        cell = [[CardTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    }
    
    // Configure the cell...
    [self configureCardCell:cell atIndexPath:indexPath];
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    Moment *moment = [_fetchedResultsController objectAtIndexPath:indexPath];
    
    //Issue :Prevents flashing on server response when using core data table
    //Check just update after 1 minute
    //Now
    /*
    NSDate *now = [NSDate date];
    NSTimeInterval secondsSince = -(int)[moment.lastUpdate timeIntervalSinceDate:now];
    if(secondsSince!= 0)
        if(secondsSince <60) return;
    */
    
    //I think need Cancel NSMutableURLRequest
    //Synch like count from server to local
    NSString *likeEndpoint = [NSString stringWithFormat:kLikeCount,moment.momentID];
    [ServerManager makeGETrequestUserActivityWithBaseURL:kBaseURL
                                    endpoint:likeEndpoint
                                  completion:^(BOOL success, NSDictionary *dict, NSError *error) {
                                      if(success && dict){
                                          //moment.lastUpdate = [NSDate date];
                                          dispatch_async(dispatch_get_main_queue(), ^(void){
                                              //Run UI Updates
                                              if(moment.like != [[dict objectForKey:@"data"] intValue]){
                                                  moment.like = [[dict objectForKey:@"data"] intValue];
                                                  [Appdelegate saveMasterContext];
                                              }
                                          });
                                          
                                      }
                                    }];
    
    //Synch comment count from server to local
    NSString *commentEndpoint = [NSString stringWithFormat:kCommentCount,moment.momentID];
    [ServerManager makeGETrequestUserActivityWithBaseURL:kBaseURL
                                                endpoint:commentEndpoint
                                              completion:^(BOOL success, NSDictionary *dict, NSError *error) {
                                                  if(success && dict){
                                                      //moment.lastUpdate = [NSDate date];
                                                      if(moment.comment != [[dict objectForKey:@"data"] intValue])
                                                      dispatch_async(dispatch_get_main_queue(), ^(void){
                                                          //Run UI Updates
                                                          moment.comment = [[dict objectForKey:@"data"] intValue];
                                                          [Appdelegate saveMasterContext];
                                                      });
                                                      
                                                  }
                                              }];
    
}

- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {

    Moment *moment = [_fetchedResultsController objectAtIndexPath:indexPath];

    // Ensure ongoingDownloadOperation is not nil
    //Fetch operation that doesn't need executing anymore
    NSBlockOperation *ongoingProfileDownloadOperation = [_userProfileImageDownloadOperations objectForKey:moment.user.userID];
    if (ongoingProfileDownloadOperation) {
        //Cancel operation and remove from dictionary
        [ongoingProfileDownloadOperation cancel];
        [_userProfileImageDownloadOperations removeObjectForKey:moment.user.userID];
    }
    
    NSBlockOperation *ongoingMainImageDownloadOperation = [_mainImageDownloadOperations objectForKey:moment.momentID];
    if (ongoingProfileDownloadOperation) {
        //Cancel operation and remove from dictionary
        [ongoingMainImageDownloadOperation cancel];
        [_mainImageDownloadOperations removeObjectForKey:moment.momentID];
    }
    
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

}

#pragma mark - UITableViewDelegate

-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section{

    return 1.0;
}


-(CGFloat)tableView:(UITableView*)tableView heightForFooterInSection:(NSInteger)section{
    return 1.0;
}

-(UIView*)tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
}

-(UIView*)tableView:(UITableView*)tableView viewForFooterInSection:(NSInteger)section{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
}

#pragma mark - NSFetchedResultsControllerDelegate

- (NSFetchedResultsController *)fetchedResultsController {
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Moment" inManagedObjectContext:[Appdelegate managedObjectContext]];
    [fetchRequest setEntity:entity];
    
    NSSortDescriptor *sort = [[NSSortDescriptor alloc]
                              initWithKey:@"postedAt" ascending:NO];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sort]];
    
    [fetchRequest setFetchBatchSize:20];
    
    NSFetchedResultsController *theFetchedResultsController =
    [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                        managedObjectContext:[Appdelegate managedObjectContext] sectionNameKeyPath:nil
                                                   cacheName:@"root"];
    self.fetchedResultsController = nil;
    self.fetchedResultsController = theFetchedResultsController;
    _fetchedResultsController.delegate = self;
    
    return _fetchedResultsController;
    
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    // The fetch controller is about to start sending change notifications, so prepare the table view for updates.
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id )sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
        default:
            break;
            
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = self.tableView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCardCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray
                                               arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray
                                               arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    // The fetch controller has sent all current change notifications, so tell the table view to process all updates.
    
    [self.tableView endUpdates];
}
@end
